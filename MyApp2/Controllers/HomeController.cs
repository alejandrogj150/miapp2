﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyApp2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public int regresaUnValor()
        {
            var random = new Random();
            int unValor = random.Next();
            bool menor_a_millon = false;
            if (unValor > 0 && unValor < 1000000)
            {
                menor_a_millon = true;
            }

            return unValor;
        }

        public string Letras()
        {
            string letras = string.Empty;
            string abc = "abc";
            string def = "def";
            string ghi = "ghi";

            var random = new Random();
            int unValor = random.Next(3);

            if (unValor <= 1)
            {
                letras = letras + abc;
            }
            if (unValor <= 2)
            {
                letras = letras + def;
            }
            if (unValor <= 3)
            {
                letras = letras + ghi;
            }




            return letras;
        }

        public ActionResult Blog()
        {            
            int miNumero = regresaUnValor();
            string letras = Letras();

            ViewBag.MiBlog = "Blog sobre tecnología";
            ViewBag.miNumero = miNumero;
            ViewBag.letras = letras;

            return View();
        }
    }
}